package main

import (
	"encoding/json"
	"log"
	"net/http"
	"time"
)

const (
	defaultFormatDate = "2006-01-02"
)

// Installment stage
const (
	MitraStage   = iota + 1 // 1
	AgentStage              // 2
	ReqToFOStage            // 3
	FOStage                 // 4
	BMStage                 // 5
	ApproveStage            // 6
)

// User Reference
const (
	Mitra = iota + 1 // 1
	Agent            // 2
	FO               // 3
	BM               // 4
)

// SuccessResponse - stored information for success response
type SuccessResponse struct {
	Status  int                    `json:"status"`
	Code    int                    `json:"code"`
	Message string                 `json:"message"`
	Data    map[string]interface{} `json:"data"`
}

// ErrorResponse - stored information for success response
type ErrorResponse struct {
	Status  int                    `json:"status"`
	Code    int                    `json:"code"`
	Message string                 `json:"message"`
	Error   map[string]interface{} `json:"error"`
}

// WriteSuccessResponse - write success response to the client
func WriteSuccessResponse(wr http.ResponseWriter, successResp SuccessResponse) {
	wr.Header().Set("Content-Type", "application/json")
	json.NewEncoder(wr).Encode(successResp)
}

// WriteErrorResponse - write error response to the client
func WriteErrorResponse(wr http.ResponseWriter, errResp ErrorResponse) {
	byteStruct, err := json.Marshal(errResp)
	if err != nil {
		log.Println("Error: ", err)
	}
	wr.Header().Set("Content-Type", "application/json")
	wr.WriteHeader(errResp.Status)
	wr.Write(byteStruct)
}

// SetErrorResponse - set all properties error response
func SetErrorResponse(code int, statusCode int, message string, err map[string]interface{}) ErrorResponse {
	errResp := ErrorResponse{
		Status:  statusCode,
		Code:    code,
		Message: message,
		Error:   err,
	}
	return errResp
}

// SetSuccessResponse - set all properties success response
func SetSuccessResponse(status int, code int, message string, data map[string]interface{}) SuccessResponse {
	succResp := SuccessResponse{
		Status:  status,
		Code:    code,
		Message: "Success",
		Data:    data,
	}
	return succResp
}

// StringToDate - String to date conversion
func StringToDate(strDate string) (time.Time, error) {
	return time.Parse(defaultFormatDate, strDate)
}
