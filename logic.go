package main

import (
	"errors"
	"log"
	"net/http"
	"strings"
)

// GetIntStage - return integer mapping stage of installment
// 1 -> AGENT
// 2 -> REQ_TO_FO
// 3 -> FO
// 4 -> BM
func GetIntStage(userType string) (int, error) {
	switch strings.ToUpper(userType) {
	case "AGENT":
		return 1, nil
	case "REQ_TO_FO":
		return 2, nil
	case "FO":
		return 3, nil
	case "BM":
		return 4, nil
	default:
		return -1, errors.New("Invalid user type")
	}
}

func isValidHistoryType(historyType string) bool {
	if strings.ToUpper(historyType) == "FROM_MITRA" || strings.ToUpper(historyType) == "TO_FO" {
		return true
	}
	return false
}

// FindTxHistoryAgentFromMitra - Get history transaction data agent from mitra
func (db *DB) FindTxHistoryAgentFromMitra(userID uint64, startDate, endDate string) ([]TransactionHistoryAgentFromMitra, ErrorResponse) {
	// Intialize variables to zero values
	var errResp ErrorResponse
	var histories []TransactionHistoryAgentFromMitra

	rows, err := db.Query(QAgentTxHistoryFromMitra, userID, startDate, endDate)
	if err != nil {
		log.Println("[ERROR] ", err)
		dataErr := map[string]interface{}{"error": err.Error()}
		return nil, SetErrorResponse(http.StatusInternalServerError, http.StatusInternalServerError, "Error", dataErr)
	}
	defer rows.Close()
	for rows.Next() {
		var history TransactionHistoryAgentFromMitra
		err := rows.Scan(&history.BorrowerName, &history.Group, &history.Amount, &history.Frequency,
			&history.Date, &history.LoanType, &history.TransactionCode)
		if err != nil {
			log.Println("Unable to scan property from DB to struct ", err)
			dataErr := map[string]interface{}{"message": err.Error()}
			return nil, SetErrorResponse(http.StatusInternalServerError, http.StatusInternalServerError, "Error", dataErr)
		}
		histories = append(histories, history)
	}
	if rows.Err() != nil {
		dataErr := map[string]interface{}{"error": err.Error()}
		return nil, SetErrorResponse(http.StatusInternalServerError, http.StatusInternalServerError, "Error", dataErr)
	}
	return histories, errResp
}

// FindTXHistoryFromAgentToFO - Function to retrive transaction history
// From Agent to FO
func (db *DB) FindTXHistoryFromAgentToFO(userID uint64, startDate, endDate string) ([]TXAgentToFO, ErrorResponse) {
	var errResp ErrorResponse
	var histories []TXAgentToFO
	rows, err := db.Query(QAgentToFO, userID, startDate, endDate)
	if err != nil {
		log.Println("[ERROR] ", err)
		dataErr := map[string]interface{}{"error": err.Error()}
		return nil, SetErrorResponse(http.StatusInternalServerError, http.StatusInternalServerError, "Error", dataErr)
	}
	defer rows.Close()
	for rows.Next() {
		var history TXAgentToFO
		err := rows.Scan(&history.Date, &history.FOName, &history.Amount)
		if err != nil {
			log.Println("Unable to scan property from DB to struct ", err)
			dataErr := map[string]interface{}{"message": err.Error()}
			return nil, SetErrorResponse(http.StatusInternalServerError, http.StatusInternalServerError, "Error", dataErr)
		}
		histories = append(histories, history)
	}
	if rows.Err() != nil {
		dataErr := map[string]interface{}{"error": err.Error()}
		return nil, SetErrorResponse(http.StatusInternalServerError, http.StatusInternalServerError, "Error", dataErr)
	}
	return histories, errResp
}

// CountTransactions - Conunt history of transaction based on date
func (db *DB) CountTransactions(userID uint64, userFromType uint64, userToType uint64, startDate string, endDate string) (TotalHistory, ErrorResponse) {
	var totalHistory TotalHistory
	var errResp ErrorResponse

	rows, err := db.Query(QCountHistory, userID, userFromType, userToType, startDate, endDate)
	if err != nil {
		dataErr := map[string]interface{}{"error": err.Error()}
		return totalHistory, SetErrorResponse(http.StatusInternalServerError, http.StatusInternalServerError, "Error", dataErr)
	}
	defer rows.Close()
	for rows.Next() {
		err := rows.Scan(&totalHistory.Total)
		if err != nil {
			log.Println("Unable to scan property from DB to struct ", err)
			dataErr := map[string]interface{}{"message": err.Error()}
			return totalHistory, SetErrorResponse(http.StatusInternalServerError, http.StatusInternalServerError, "Error", dataErr)
		}
	}
	if rows.Err() != nil {
		dataErr := map[string]interface{}{"message": err.Error()}
		return totalHistory, SetErrorResponse(http.StatusInternalServerError, http.StatusInternalServerError, "Error", dataErr)
	}
	return totalHistory, errResp
}

// FindTransactionHistoryFromFOToBM - find transactions history from FO to Agent
func (db *DB) FindTransactionHistoryFromFOToBM(userID uint64, startDate, endDate string) ([]TXFOToBM, ErrorResponse) {
	var histories []TXFOToBM
	var errResp ErrorResponse

	rows, err := db.Query(QFOToBM, userID, startDate, endDate)
	if err != nil {
		dataErr := map[string]interface{}{"error": err.Error()}
		return nil, SetErrorResponse(http.StatusInternalServerError, http.StatusInternalServerError, "Error", dataErr)
	}
	for rows.Next() {
		var history TXFOToBM
		err := rows.Scan(&history.TransactionType, &history.Name, &history.Amout, &history.Date)
		if err != nil {
			log.Println("Unable to scan property from DB to struct ", err)
			dataErr := map[string]interface{}{"message": err.Error()}
			return nil, SetErrorResponse(http.StatusInternalServerError, http.StatusInternalServerError, "Error", dataErr)
		}
		histories = append(histories, history)
	}
	if rows.Err() != nil {
		dataErr := map[string]interface{}{"message": err.Error()}
		return nil, SetErrorResponse(http.StatusInternalServerError, http.StatusInternalServerError, "Error", dataErr)
	}
	return histories, errResp
}
