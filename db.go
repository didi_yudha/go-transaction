package main

import (
	"database/sql"

	_ "github.com/lib/pq"
)

// Datastore - define all
type Datastore interface {
	FindTxHistoryAgentFromMitra(userID uint64) ([]TransactionHistoryAgentFromMitra, ErrorResponse)
	FindTransactionHistories(userID, userFromType, userToType uint64) ([]TransactionHistories, ErrorResponse)
	CountTransactions(userID uint64, userFromType uint64, userToType uint64, startDate string, endDate string) (TotalHistory, ErrorResponse)
}

// DB - struct to store db driver
type DB struct {
	*sql.DB
}

// NewDB - Instantiate and check connection to Postgre database
func NewDB(c *Config) (*sql.DB, error) {
	db, err := sql.Open(c.GetDriverDBName(), c.GetDatasource())
	if err != nil {
		return nil, err
	}
	if err = db.Ping(); err != nil {
		return nil, err
	}
	return db, nil
}
