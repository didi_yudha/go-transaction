package main

import (
	"net/http"

	"github.com/gorilla/mux"
)

const (
	prefixURI = "/api/v1/transactions/"
)

// NewRouter - Instantiate all routers for go-transaction service
func NewRouter() *mux.Router {
	router := mux.NewRouter()
	router.
		Methods(http.MethodGet, http.MethodOptions).
		Path(prefixURI + "agent/{user_id}/installment-history/from-mitra/{start_date}/{end_date}").
		HandlerFunc(GetTransactionHistoryFromMitra)
	router.
		Methods(http.MethodGet, http.MethodOptions).
		Path(prefixURI + "agent/{user_id}/installment-history/to-agent/{start_date}/{end_date}").
		HandlerFunc(GetTransactionHistoryToAgent)
	router.
		Methods(http.MethodGet, http.MethodOptions).
		Path(prefixURI + "count/{user_id}/{user_from_type}/{user_to_type}/{start_date}/{end_date}").
		HandlerFunc(CountTransactionHistory)
	router.
		Methods(http.MethodGet, http.MethodOptions).
		Path(prefixURI + "fo/{user_id}/history/{start_date}/{end_date}").
		HandlerFunc(GetTransactionFromFOToBM)
	return router
}
