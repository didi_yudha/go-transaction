package main

import (
	"fmt"
	"log"
	"net/http"
)

func init() {
	ReadConfig(&config)
	SetEnvironment(&env)
}

func main() {
	router := NewRouter()
	fmt.Println("GO Transaction service is up on port ", config.Port)
	log.Fatalln(http.ListenAndServe(":"+config.Port, router))
}
