package main

import (
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

// GetTransactionHistoryFromMitra - Get from history from mitra
func GetTransactionHistoryFromMitra(wr http.ResponseWriter, req *http.Request) {
	params := mux.Vars(req)
	// Get user ID param
	userIDParam := params["user_id"]
	startDate := params["start_date"]
	endDate := params["end_date"]
	log.Println("[INFO] userIDParam:", userIDParam)
	log.Println("[INFO] startDate: ", startDate)
	log.Println("[INFO] endDate: ", startDate)
	// Convert user id params to uint64
	userID, err := strconv.ParseUint(userIDParam, 10, 64)
	if err != nil {
		dataErr := map[string]interface{}{"message": "Invalid user ID"}
		WriteErrorResponse(wr, SetErrorResponse(http.StatusBadRequest, http.StatusBadRequest, "Error", dataErr))
		return
	}
	log.Println("[INFO] userID: ", userID)
	histFromMitra, errResp := dbHelper.FindTxHistoryAgentFromMitra(userID, startDate, endDate)
	if errResp.Code != 0 {
		WriteErrorResponse(wr, errResp)
		return
	}
	dataHistory := map[string]interface{}{"transactionHistories": histFromMitra}
	WriteSuccessResponse(wr, SetSuccessResponse(http.StatusOK, http.StatusOK, "Success", dataHistory))
}

// GetTransactionHistoryToAgent - Get transaction histories assigned to agent
func GetTransactionHistoryToAgent(wr http.ResponseWriter, req *http.Request) {
	hist := make([]TXAgentToFO, 0)
	params := mux.Vars(req)
	// Get user ID param
	userIDParam := params["user_id"]
	startDate := params["start_date"]
	endDate := params["end_date"]

	log.Println("[INFO] userIDparam: ", userIDParam)
	log.Println("[INFO] startDate: ", startDate)
	log.Println("[INFO] endDate: ", endDate)

	userID, err := strconv.ParseUint(userIDParam, 10, 64)
	if err != nil {
		dataErr := map[string]interface{}{"message": "Invalid user_from param"}
		WriteErrorResponse(wr, SetErrorResponse(http.StatusBadRequest, http.StatusBadRequest, "Error", dataErr))
		return
	}
	hist, errResp := dbHelper.FindTXHistoryFromAgentToFO(userID, startDate, endDate)
	if errResp.Code != 0 {
		dataErr := map[string]interface{}{"message": err.Error()}
		WriteErrorResponse(wr, SetErrorResponse(http.StatusBadRequest, http.StatusBadRequest, "Error", dataErr))
		return
	}
	data := map[string]interface{}{"transactionHistories": hist}
	WriteSuccessResponse(wr, SetSuccessResponse(http.StatusOK, http.StatusOK, "Success", data))
}

// CountTransactionHistory - Count transaction of an user by date
func CountTransactionHistory(wr http.ResponseWriter, req *http.Request) {
	params := mux.Vars(req)
	// Get all url parameters
	userIDParam := params["user_id"]
	userFrom := params["user_from_type"]
	userTo := params["user_to_type"]
	strDateParam := params["start_date"]
	endDateParam := params["end_date"]

	log.Println("[INFO] userIDParam: ", userIDParam)
	log.Println("[INFO] userFrom: ", userFrom)
	log.Println("[INFO] userTo: ", userTo)
	log.Println("[INFO] strDateParam: ", strDateParam)
	log.Println("[INFO] endDateParam: ", endDateParam)

	// Convert user id params to uint64
	userID, err := strconv.ParseUint(userIDParam, 10, 64)
	if err != nil {
		log.Println(err)
		dataErr := map[string]interface{}{"message": "Invalid user_id param"}
		WriteErrorResponse(wr, SetErrorResponse(http.StatusBadRequest, http.StatusBadRequest, "Error", dataErr))
		return
	}

	userFromType, err := strconv.ParseUint(userFrom, 10, 64)
	log.Println("[INFO] userFrom", userFrom)
	if err != nil {
		log.Println("[ERROR] ", err)
		dataErr := map[string]interface{}{"message": "Invalid user_from param"}
		WriteErrorResponse(wr, SetErrorResponse(http.StatusBadRequest, http.StatusBadRequest, "Error", dataErr))
		return
	}

	// Convert user to param to uint64
	userToType, err := strconv.ParseUint(userTo, 10, 64)
	if err != nil {
		dataErr := map[string]interface{}{"message": "Invalid user_to param"}
		WriteErrorResponse(wr, SetErrorResponse(http.StatusBadRequest, http.StatusBadRequest, "Error", dataErr))
		return
	}

	_, err = StringToDate(strDateParam)
	if err != nil {
		dataErr := map[string]interface{}{"message": "Invalid start_date param"}
		WriteErrorResponse(wr, SetErrorResponse(http.StatusBadRequest, http.StatusBadRequest, "Error", dataErr))
		return
	}

	_, err = StringToDate(endDateParam)
	if err != nil {
		dataErr := map[string]interface{}{"message": "Invalid end_date param"}
		WriteErrorResponse(wr, SetErrorResponse(http.StatusBadRequest, http.StatusBadRequest, "Error", dataErr))
		return
	}
	totalHistory, errResp := dbHelper.CountTransactions(userID, userFromType, userToType, strDateParam, endDateParam)
	if errResp.Code != 0 {
		WriteErrorResponse(wr, errResp)
		return
	}
	dataHistory := map[string]interface{}{"totalTransactions": totalHistory.Total}
	WriteSuccessResponse(wr, SetSuccessResponse(http.StatusOK, http.StatusOK, "Success", dataHistory))
}

// GetTransactionFromFOToBM - Get transaction history from FO to BM
func GetTransactionFromFOToBM(wr http.ResponseWriter, req *http.Request) {
	params := mux.Vars(req)
	userIDparam := params["user_id"]
	startDate := params["start_date"]
	endDate := params["end_date"]

	// Convert user id params to uint64
	userID, err := strconv.ParseUint(userIDparam, 10, 64)
	if err != nil {
		log.Println(err)
		dataErr := map[string]interface{}{"message": "Invalid user_id param"}
		WriteErrorResponse(wr, SetErrorResponse(http.StatusBadRequest, http.StatusBadRequest, "Error", dataErr))
		return
	}

	histories, errResp := dbHelper.FindTransactionHistoryFromFOToBM(userID, startDate, endDate)
	if errResp.Code != 0 {
		WriteErrorResponse(wr, errResp)
		return
	}
	dataHistory := map[string]interface{}{"transactionHistories": histories}
	WriteSuccessResponse(wr, SetSuccessResponse(http.StatusOK, http.StatusOK, "Success", dataHistory))
}
