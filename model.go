package main

// TransactionHistoryAgentFromMitra - Retrieve transaction history from mitra to agent
type TransactionHistoryAgentFromMitra struct {
	BorrowerName    string  `json:"borrowerName"`
	Group           string  `json:"groupName"`
	Amount          float64 `json:"amount"`
	Frequency       float64 `json:"frequency"`
	Date            string  `json:"date"`
	TransactionCode string  `json:"transactionCode"`
	LoanType        string  `json:"loanType"`
}

// TransactionHistories - Retrieve transaction histories
type TransactionHistories struct {
	Date         string  `json:"date"`
	BorrowerName string  `json:"borrowerName"`
	Group        string  `json:"groupName"`
	Amount       float64 `json:"amount"`
}

// TXAgentToFO - store transaction history information
// From Agent to FO
type TXAgentToFO struct {
	Date   string  `json:"date"`
	FOName string  `json:"borrowerName"`
	Group  string  `json:"groupName"`
	Amount float64 `json:"amount"`
}

// TotalHistory - store count total history information
type TotalHistory struct {
	Total int64 `json:"total"`
}

// InstallmentStage - store installment stage information
type InstallmentStage struct {
	ID   uint64
	Name string
}

// UserReference - store user reference information
type UserReference struct {
	ID   uint64
	Name string
}

// TXFOToBM - store fo to bm transaction information
type TXFOToBM struct {
	TransactionType string  `json:"transactionType"`
	Name            string  `json:"name"`
	Amout           float64 `json:"amount"`
	Date            string  `json:"date"`
}
