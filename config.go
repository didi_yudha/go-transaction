package main

import (
	"database/sql"
	"io/ioutil"
	"log"

	"gopkg.in/yaml.v2"
)

const (
	fileConfig = "config.yaml"
)

var (
	config   Config
	env      Env
	dbHelper DB
)

// Env - Set all environments that are needed
// Attach interface, so that we can mock it
type Env struct {
	db *sql.DB
}

// Config - System configuration
type Config struct {
	Port string
	Psql
}

// Psql - Configuration of Postgre SQL database
type Psql struct {
	Db       string `yaml:"db"`
	Name     string `yaml:"name"`
	Host     string `yaml:"host"`
	Port     string `yaml:"port"`
	Username string `yaml:"username"`
	Password string `yaml:"password"`
	SslMode  string `yaml:"ssl_mode"`
}

// ReadConfig - Read yaml configuration file
func ReadConfig(c *Config) {
	ymlFile, err := ioutil.ReadFile(fileConfig)
	if err != nil {
		log.Fatalln(err)
	}
	err = yaml.Unmarshal(ymlFile, c)
	if err != nil {
		log.Fatalln(err)
	}
}

// SetEnvironment - Set environment system, eg: Database
func SetEnvironment(env *Env) {
	db, err := NewDB(&config)
	if err != nil {
		log.Panic(err)
	}
	env.db = db
	dbHelper.DB = db
}

// GetDriverDBName - return driver database name in used
func (c Config) GetDriverDBName() string {
	return c.Psql.Name
}

// GetDatasource - return datasource name database in used
func (c Config) GetDatasource() string {
	return c.Psql.Name + "://" + c.Psql.Username + ":" + c.Psql.Password + "@" + c.Psql.Host + "/" + c.Psql.Db + "?sslmode=" + c.Psql.SslMode
}
