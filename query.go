package main

var (

	// QAgentTxHistoryFromMitra - Get data installment by agent id and stage
	QAgentTxHistoryFromMitra = ` select cif."name" as "borrowerName",
									g."name" as "groupName",
									icp.amount as "amount",
									icp.frequency as "frequency",
									icp."createdAt" as "date",
									l."loanType" as "loanType",
									concat(LPAD(icp.id::text, 6,'0'),LPAD(icp.frequency::text, 2, '0')) as "transactionCode"
								from installment_cash_point icp
									join transaction_cash_point tcp on icp.id = ANY(tcp."installmentId")
									join loan l on l.id = icp."loanId" 
									join r_loan_borrower rlb on rlb."loanId" = l.id
									join borrower on borrower.id = rlb."borrowerId"
									join r_cif_borrower rcb on rcb."borrowerId" = borrower.id
									join cif on cif.id = rcb."cifId"
									join r_loan_group rlg on rlg."loanId" = l.id
									join "group" g on g.id = rlg."groupId"
								where icp.stage = 2 and 
								tcp."userToId" = $1 and
								tcp."userToType"= 2 and 
								icp."transactionDate"::date between $2 and $3 and tcp.stage = 2 `

	// QAgentToFO - Query to get data transaction from AGENT to FO
	QAgentToFO = ` select tcp."createdAt" as "date",
						agent.fullname,
						tcp.amount as "amount"
					from transaction_cash_point tcp
						join agent on agent.id = tcp."userToId"
					where tcp."userFromId" = $1 and 
					tcp."userFromType" = 2 and 
					tcp."userToType" = 3 and 
					tcp."createdAt"::date between $2 and $3 and 
					UPPER(agent._role) = UPPER('FO') and
					tcp."deletedAt" isnull and tcp.stage = 2
					order by tcp."createdAt" desc `

	// QCountHistory - Count transaction history
	QCountHistory = ` select count(*) as "numberOfHistory" 
							from transaction_cash_point 
						where
							"userToId" = $1 and
							"userFromType" = $2 and 
							"userToType" = $3 and 
							"createdAt"::date between $4 and $5 and 
						"deletedAt" isnull and stage = 2 `

	// QFOToBM - Get data transaction history from FO to BM
	QFOToBM = `select 'Agent' as "type", aa.fullname, tcp.amount, tcp."createdAt" "createdAt"
					from transaction_cash_point tcp 
					join agent aa on aa.id = tcp."userFromId" and "userFromType" = 2
				where tcp."deletedAt" isnull and "userToType" = 3 and "userToId" = $1 	
					and tcp."createdAt"::date between $2 and $3  
					union all
				select 'BM' as "type", um.fullname, tcp.amount, tcp."createdAt"
					from transaction_cash_point tcp 
					join user_mis um on um.id = tcp."userToId" and "userToType" = 4
				where tcp."deletedAt" isnull and "userFromType" = 3 and "userFromId" = $1
					and tcp."createdAt"::date between $2 and $3
				order by "createdAt" desc`
)
